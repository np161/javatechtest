package com.techtest.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.techtest.dao.AnimalDao;
import com.techtest.model.Animal;

@Repository
public class AnimalDaoImpl implements AnimalDao {

	@Autowired
	private SessionFactory session;
	
	@Override
	public void add(Animal animal) {
		session.getCurrentSession().save(animal);
	}

	@Override
	public void edit(Animal animal) {
		session.getCurrentSession().update(animal);
	}

	@Override
	public void delete(int animalId) {
		session.getCurrentSession().delete(getAnimal(animalId));
	}

	@Override
	public Animal getAnimal(int animalId) {
		return (Animal)session.getCurrentSession().get(Animal.class, animalId);
	}

	@Override
	public List getAllAnimals() {
		return session.getCurrentSession().createQuery("from Animal").list();
	}

}
