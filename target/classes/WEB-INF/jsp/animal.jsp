<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file ="/WEB-INF/jsp/includes.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<title class="col-md-6">Animal Manager</title>
</head>
<h1>Animal Information</h1>
<div class= "col-md-12 table-bordered">
<table border="1" class = "table">
	<th>Animal ID</th>
	<th>Name</th>
	<th>Weight</th>
	<th>Health Status</th>
	<th>Type</th>
	<c:forEach items="${animalList}" var="animal">
		<tr>
			<td>${animal.id}</td>
			<td>${animal.name}</td>
			<td>${animal.weight}</td>
			<td>${animal.health}</td>
			<td>${animal.type}</td>
		</tr>
	</c:forEach>
</table>
</div>
<br>

<h1>Animal Management</h1>
<div class="container">
    <div class="row">
    
    <!-- Add Form Div -->
        <div class="col-md-4">
           <h2>Add Animal</h2>
           <form:form action="animal.do" method="POST" commandName="animal">
              	<div class="form-group">
    				<label>Name</label>
    				<form:input path="name" type="text" lclass="form-control" placeholder="Name"/>
  				</div>
  				<div class="form-group">
    				<label>Weight</label>
    				<form:input path="weight" type="number" lclass="form-control" placeholder="Weight(kg)"/>
  				</div>
  				<div class="form-group">
    				<label>Health Status</label>
    				<form:input path="health" type="text" lclass="form-control" placeholder="Health Status"/>
    			</div>
  				<div class="form-group">
    				<label>Type</label>
    				<form:input path="type" type="text" lclass="form-control" placeholder="Type"/>
  				</div>
  				<div class="btn-group">
					<input type="submit" style="padding: 10"class="btn btn-primary" name="action" value="Add" />
				</div>
				</form:form>

        </div>
        
        
        <!-- Edit Form Div -->
        <div class="col-md-4">
            <h2>Edit Animal</h2>
            <form:form action="animal.do" method="POST" commandName="animal">
		<div class="form-group">
    				<label>Animal ID</label>
    				<form:input path="id" type="number" lclass="form-control" placeholder="ID to edit"/>
  				</div>
		<div class="form-group">
    				<label>Name</label>
    				<form:input path="name" type="text" lclass="form-control" placeholder="Name"/>
  				</div>
  				<div class="form-group">
    				<label>Weight</label>
    				<form:input path="weight" type="number" lclass="form-control" placeholder="Weight(kg)"/>
  				</div>
  				<div class="form-group">
    				<label>Health Status</label>
    				<form:input path="health" type="text" lclass="form-control" placeholder="Health Status"/>
    			</div>
  				<div class="form-group">
    				<label>Type</label>
    				<form:input path="type" type="text" lclass="form-control" placeholder="Type"/>
  				</div>
  				<div class="btn-group">
					<input type="submit" style="padding: 10"class="btn btn-primary" name="action" value="Edit" />
				</div>
</form:form>
</div>
        
        <!-- Delete/Search Form Div -->
        <div class="col-md-4">
            <h2>Delete/Search Animal</h2>
            	<form:form action="animal.do" method="POST" commandName="animal">
              	<div class="form-group">
    				<label>Animal ID</label>
    				<form:input path="Id" type="number" lclass="form-control" placeholder="Animal ID"/>
  				</div>
  				<div class="btn-group">
					<input type="submit" style="padding: 10"class="btn btn-primary" name="action" value="Delete" />
					<input type="submit" style="padding: 10" class="btn btn-primary" name="action" value="Search" />
				</div>
				</form:form>
        </div>
    </div>
</div>


</body>
</html>