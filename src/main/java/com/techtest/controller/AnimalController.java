package com.techtest.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techtest.model.Animal;
import com.techtest.service.AnimalService;

@Controller
public class AnimalController {

	@Autowired
	private AnimalService animalService;
	
	@RequestMapping("/index")
	private String setupForm(Map<String, Object> map){
		Animal animal = new Animal();
		map.put("animal", animal);
		map.put("animalList", animalService.getAllAnimals());
		return "animal";
	}
	
	@RequestMapping(value="/animal.do", method = RequestMethod.POST)
	public String doActions(@ModelAttribute Animal animal, BindingResult result, @RequestParam String action, Map<String, Object> map ){
		Animal animalResult = new Animal();
		
		switch(action.toLowerCase()){
		case "add":
			animalService.add(animal);
			animalResult = animal;
			break;
		case "edit":
			animalService.edit(animal);
			animalResult = animal;
			break;
		case "delete":
			animalService.delete(animal.getId());
			animalResult = new Animal();
			break;
		case "search":
			Animal searchedAnimal = animalService.getAnimal(animal.getId());
			animalResult = searchedAnimal != null ? searchedAnimal : new Animal();
			break;
		}
		
		map.put("animal", animalResult);
		map.put("animalList", animalService.getAllAnimals());
		
		return "animal";
	}
}
