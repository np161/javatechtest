package com.techtest.model;


import javax.persistence.Entity;

@Entity
public class MountainGoat extends Goat{
	
	private String mountainRange;

	public MountainGoat(){};
	
	public MountainGoat(int id, String name, int weight, String health, boolean milked, String mountainRange) {
		super(id, name, weight, health, "Mountain Goat", milked);
		this.mountainRange = mountainRange;
	}

	public String getMountainRange() {
		return mountainRange;
	}

	public void setMountainRange(String mountainRange) {
		this.mountainRange = mountainRange;
	}
	
}
