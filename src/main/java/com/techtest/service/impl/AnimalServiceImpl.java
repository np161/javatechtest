package com.techtest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techtest.dao.AnimalDao;
import com.techtest.model.Animal;
import com.techtest.service.AnimalService;

@Service
public class AnimalServiceImpl implements AnimalService {

	@Autowired
	private AnimalDao animalDao;
	
	@Transactional
	public void add(Animal animal) {
		animalDao.add(animal);
	}

	@Transactional
	public void edit(Animal animal) {
		animalDao.edit(animal);
	}
		
	@Transactional
	public void delete(int animalId) {
		animalDao.delete(animalId);
	}

	@Transactional
	public Animal getAnimal(int animalId) {
		return animalDao.getAnimal(animalId);
	}

	@Transactional
	public List getAllAnimals() {
		return animalDao.getAllAnimals();
	}

}
