package com.techtest.dao;

import java.util.List;

import com.techtest.model.Animal;

public interface AnimalDao {
	public void add(Animal animal);
	public void edit(Animal animal);
	public void delete(int animalId);
	public Animal getAnimal(int animalId);
	public List getAllAnimals();
}
