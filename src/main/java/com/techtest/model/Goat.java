package com.techtest.model;


import javax.persistence.Entity;

@Entity
public class Goat extends Animal{

	private boolean milked;

	public Goat(){};
	
	public Goat(int id, String name, int weight, String health, boolean milked) {
		super(id, name, weight, health, "Goat");
		this.milked = milked;
	}

	public Goat(int id, String name, int weight, String health, String type, boolean milked) {
		super(id, name, weight, health, type);
		this.milked = milked;
	}

	public boolean isMilked() {
		return milked;
	}

	public void setMilked(boolean milked) {
		this.milked = milked;
	}
	
	
	
	
}
