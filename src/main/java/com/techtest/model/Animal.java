package com.techtest.model;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Animal {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id;
	@Column
	private String name;
	@Column
	private int weight;
	@Column
	private String health;
	@Column
	private String type;
	
	public Animal(){};
	
	public Animal(int id, String name, int weight, String health, String type) {
		super();
		Id = id;
		this.name = name;
		this.weight = weight;
		this.health = health;
		this.type = type;
	}
	
	//Getters Setters
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getHealth() {
		return health;
	}
	public void setHealth(String health) {
		this.health = health;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	

}
