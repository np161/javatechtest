package com.techtest.service;

import java.util.List;

import com.techtest.model.Animal;

public interface AnimalService {
	public void add(Animal animal);
	public void edit(Animal animal);
	public void delete(int animalId);
	public Animal getAnimal(int animalId);
	public List getAllAnimals();
}
