package com.techtest.model;

import javax.persistence.Entity;

@Entity
public class Cow extends Animal {
	
	private boolean readyForSlaughter;
	
	public Cow(){};

	public Cow(int id, String name, int weight, String health, boolean readyForSlaughter) {
		super(id, name, weight, health, "Cow");
		this.readyForSlaughter = readyForSlaughter;
	}

	public boolean isReadyForSlaughter() {
		return readyForSlaughter;
	}

	public void setReadyForSlaughter(boolean readyForSlaughter) {
		this.readyForSlaughter = readyForSlaughter;
	}
	

	
	
	
}
